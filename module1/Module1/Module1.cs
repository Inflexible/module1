﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module1 = new Module1();
            module1.SwapItems(1, 2);
            module1.GetMinimumValue(new[] { 10, 20, 1 });
        }

        public int[] SwapItems(int a, int b)
        {
            a = a + b;
            b = a - b;
            a = a - b;
            Console.WriteLine("a = {0}, b = {1}", a, b);
            return new int[] { a, b };
        }

        public int GetMinimumValue(int[] input)
        {
            int z = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                if (z > input[i]) z = input[i];
            }
            Console.WriteLine(z);
            return z;
        }
    }
}